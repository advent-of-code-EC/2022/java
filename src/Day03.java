import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Day03 {
    //Variables used across all functions
    private File input;

    private String[] inputAsArray = new String[1000000];
    private String[][] inputManipulated = new String[1000000][2];
    private int inputSize;

    private HashMap<Character, Integer> itemsToPriority = new HashMap<>();

    //Public functions
    public void opening(String filename){
        this.input = new File(filename);
        this.inputSize = this.fileReader();
        this.priorityGenerator();
        this.inputArrayParser();
    }

    public int part1(){
        int rValue = 0;
        boolean check = true;
        char a;
        for(int i = 0; i < this.inputSize; i++){
            for(int j = 0; j < this.inputManipulated[i][0].length(); j++){
                check = true;
                a = this.inputManipulated[i][0].charAt(j);
                for(int x = 0; x < this.inputManipulated[i][1].length(); x++){
                    if(a == this.inputManipulated[i][1].charAt(x)){
                        check = false;
                        rValue += this.itemsToPriority.get(a);
                        break;
                    }
                }
                if(!check){
                    break;
                }
            }
        }
        return rValue;
    }

    //Private functions
    private int fileReader(){
        String line;
        int i = 0;
        if(this.input.isFile()){
            try (BufferedReader inputStream = new BufferedReader(new FileReader(this.input))) {
                boolean check = true;
                line = inputStream.readLine();
                while(check){
                    this.inputAsArray[i] = line;
                    i += 1;
                    line = inputStream.readLine();
                    if(line == null){
                        check = false;
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return i;
    }

    private void priorityGenerator(){
        String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(int i = 0, j = 1; i < charset.length(); i++, j++){
            this.itemsToPriority.put(charset.charAt(i), j);
        }
    }
    private void inputArrayParser(){
        for(int i = 0; i < this.inputSize; i++){
            this.inputManipulated[i][0] = this.inputAsArray[i].substring(0, ((this.inputAsArray[i].length() / 2) + 1));
            this.inputManipulated[i][1] = this.inputAsArray[i].substring(this.inputAsArray[i].length() / 2 );
        }
    }
}
