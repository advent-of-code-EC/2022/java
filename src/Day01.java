import java.io.*;

public class Day01 {
    File input = null;
    int[] input_proccessed = new int[10000000];

    public void opening(String fileName) throws FileNotFoundException {
        input = new File(fileName);
    }

    public int day1Part1() throws IOException {
        int i = 0;
        int temp = 0;
        if(input.isFile()){
            boolean check = true;
            String line;
            try (BufferedReader inputStream = new BufferedReader(new FileReader(input))) {
                line = inputStream.readLine();
                while (check) {
                    if (line.length() == 0) {
                        input_proccessed[i] = temp;
                        temp = 0;
                        i += 1;
                    } else {
                        temp += Integer.parseInt(line);
                    }
                    line = inputStream.readLine();
                    if(line == null){
                        check = false;
                        input_proccessed[i] = temp;
                        i += 1;
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        arraySort(input_proccessed);
        return input_proccessed[0];
    }

    private void arraySort(int[] array){
        int temp;
        boolean hasSwaped = true;
        while(hasSwaped) {
            hasSwaped = false;
            for (int i = 0, j = 1; j < array.length; i++, j++) {
                if(array[j] > array[i]){
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    hasSwaped = true;
                }
            }
        }

    }

    public int day01Part2(){
        int sumOfTop3 = 0;
        for(int a = 0; a < 3; a += 1){
            sumOfTop3 += input_proccessed[a];
        }
        return sumOfTop3;
    }
}
