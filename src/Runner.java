import java.io.IOException;
import java.util.Scanner;

/*
 * This will not work, I have a private runner that is how I am getting my own answers from the input.
*/
public class Runner {
    public static void main(String[] args) throws IOException {
        long startTime;
        long endTime;
        double timeTaken;
        int day;

        Scanner reader = new Scanner(System.in);
        System.out.println("Please select a day: ");
        for (int i = 1; i < 26; i += 1){
            System.out.printf("Day %d\n", i);
        }
        day = reader.nextInt();
        startTime = System.currentTimeMillis();

        switch (day){
            case 1:{
                System.out.printf("You have selected day %d\n", day);
                System.out.println("Part 1 Sample input:");
                Day01 sample = new Day01();
                String day01Sample = "[Insert your input path here]";
                sample.opening(day01Sample);
                System.out.printf("The largest amount of calories being carried by an elf is: %d\n\n", sample.day1Part1());

                System.out.println("Part 1 input:");
                Day01 input = new Day01();
                String day01Input = "[Insert your input path here]";
                input.opening(day01Input);
                System.out.printf("The largest amount of calories being carried by an elf is: %d\n\n", input.day1Part1());

                System.out.println("Part 2 sample:");
                System.out.printf("The sum of calories carried by the elves carrying the top 3 amounts of calories is %d\n", sample.day01Part2());
                System.out.println("Part 2 input:");
                System.out.printf("The sum of calories covered by the 3 elves carrying the most calories is %d\n", input.day01Part2());
            }
            case 2:{
                Day02 sample = new Day02();
                Day02 input = new Day02();
                System.out.println("Day 02 sample:");
                sample.opening("<Your own file path>");
                System.out.printf("Part 1: %d\n", sample.day2Part1());
                System.out.printf("Part 2: %d\n", sample.day2Part2());
                System.out.println("Day 02 input:");
                input.opening("<Your own file path>");
                System.out.printf("Part 1: %d\n", input.day2Part1());
                System.out.printf("Part 2: %d\n", input.day2Part2());
                break;
            }
            default:{
                System.out.println("Please rerun and select a valid day.");
            }
        }



        endTime = System.currentTimeMillis();
        timeTaken = (endTime - startTime) / 1000.0;
        System.out.printf("The program took %f seconds to complete", timeTaken);
    }
}
