import java.io.*;
import java.util.HashMap;

public class Day02 {
    File input = null;
    private char[] columnOne = new char[100000000];
    private char[] columnTwo = new char[100000000];
    private HashMap<Character, String> columnOneMap = new HashMap<Character, String>();
    private HashMap<Character, String> columnTwoMapP1 = new HashMap<Character, String>();
    private HashMap<Character, String> columnTwoMapP2 = new HashMap<Character, String>();
    private HashMap<String, Integer> usedScore = new HashMap<String, Integer>();
    private HashMap<String, Integer> outcomeScore = new HashMap<>();

    private int inputSize;

    public void opening(String fileName) throws FileNotFoundException {
        this.input = new File(fileName);
        this.inputSize = fileReader();
        this.columnOneMap.put('A', "rock");
        this.columnOneMap.put('B', "paper");
        this.columnOneMap.put('C', "scissors");
        this.columnTwoMapP1.put('X', "rock");
        this.columnTwoMapP1.put('Y', "paper");
        this.columnTwoMapP1.put('Z', "scissors");
        this.columnTwoMapP2.put('X', "loss");
        this.columnTwoMapP2.put('Y', "draw");
        this.columnTwoMapP2.put('Z', "win");
        this.usedScore.put("rock", 1);
        this.usedScore.put("paper", 2);
        this.usedScore.put("scissors", 3);
        this.outcomeScore.put("win", 6);
        this.outcomeScore.put("draw", 3);
        this.outcomeScore.put("loss", 0);
    }

    public int day2Part1(){
        int yourTotalScore = 0;
        String youPlayed;
        String opponantPlayed;
        for(int i = 0; i < this.inputSize; i++){
            opponantPlayed = this.columnOneMap.get(this.columnOne[i]);
            youPlayed = this.columnTwoMapP1.get(this.columnTwo[i]);
            yourTotalScore = yourTotalScore + this.usedScore.get(youPlayed) + matchCondition(opponantPlayed, youPlayed);
        }
        return yourTotalScore;
    }

    private int fileReader(){
        String line;
        int i = 0;
        if(this.input.isFile()){
            try (BufferedReader inputStream = new BufferedReader(new FileReader(this.input))) {
                boolean check = true;
                line = inputStream.readLine();
                while(check){
                    this.columnOne[i] = line.charAt(0);
                    this.columnTwo[i] = line.charAt(2);
                    i += 1;
                    line = inputStream.readLine();
                    if(line == null){
                        check = false;
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return i;
    }

    private int matchCondition(String Opponant, String you){
        int rValue = -1;
        switch (you){
            case "rock" ->{
                switch (Opponant) {
                    case "rock" -> {
                        rValue = 3;
                    }
                    case "paper" -> {
                        rValue = 0;
                    }
                    case "scissors" -> {
                        rValue = 6;
                    }
                }
            }
            case "paper" -> {
                switch (Opponant) {
                    case "rock" -> {
                        rValue = 6;
                    }
                    case "paper" -> {
                        rValue = 3;
                    }
                    case "scissors" -> {
                        rValue = 0;
                    }
                }
            }
            case "scissors" -> {
                switch (Opponant) {
                    case "rock" -> {
                        rValue = 0;
                    }
                    case "paper" -> {
                        rValue = 6;
                    }
                    case "scissors" -> {
                        rValue = 3;
                    }
                }
            }
        }
        return rValue;
    }

    public int day2Part2(){
        int yourTotalScore = 0;
        String opponantPlayed;
        String requiredOutcome;
        String youMustPlay = "";
        for(int i =0; i < this.inputSize; i++){
            opponantPlayed = this.columnOneMap.get(this.columnOne[i]);
            requiredOutcome = this.columnTwoMapP2.get(this.columnTwo[i]);
            switch (requiredOutcome) {
                case "loss" -> {
                    switch (opponantPlayed) {
                        case "rock" -> {
                            youMustPlay = "scissors";
                        }
                        case "paper" -> {
                            youMustPlay = "rock";
                        }
                        case "scissors" -> {
                            youMustPlay = "paper";
                        }
                    }
                }
                case "draw" -> {
                    youMustPlay = opponantPlayed;
                }
                case "win" -> {
                    switch (opponantPlayed) {
                        case "rock" -> {
                            youMustPlay = "paper";
                        }
                        case "paper" -> {
                            youMustPlay = "scissors";
                        }
                        case "scissors" -> {
                            youMustPlay = "rock";
                        }
                    }
                }
            }
            yourTotalScore += usedScore.get(youMustPlay) + this.outcomeScore.get(requiredOutcome);
        }
        return yourTotalScore;
    }
}
